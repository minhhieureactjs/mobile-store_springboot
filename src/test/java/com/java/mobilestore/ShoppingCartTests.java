package com.java.mobilestore;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

import com.java.mobilestore.entity.CartItem;
import com.java.mobilestore.entity.Product;
import com.java.mobilestore.entity.User;
import com.java.mobilestore.repository.CartItemRespository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class ShoppingCartTests {

	@Autowired
	private CartItemRespository cartRepo;
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Test
	public void testAddOneCartItem() {
		Product product = entityManager.find(Product.class, 1);
		User user = entityManager.find(User.class, 1);
		
		CartItem newItem = new CartItem();
		newItem.setProduct(product);
		newItem.setUser(user);
		newItem.setQuantity(3);
			
		CartItem saveItem = cartRepo.save(newItem);
		assertTrue(saveItem.getId() > 0);
	}
	
	@Test
	public void testGetCartItemByUser() {
		User user = new User();
		user.setId(1);
		
		List<CartItem> cartItems = cartRepo.findByUser(user);
		assertEquals(2, cartItems.size());
	}
}
