package com.java.mobilestore.service;

import java.util.List;
import java.util.Optional;

import com.java.mobilestore.entity.User;

public interface UserService {
	List<User> getAll();
	
	Optional<User> getUserById(int id);
}
