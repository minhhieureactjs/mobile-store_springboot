package com.java.mobilestore.service;

import com.java.mobilestore.entity.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    Product saveOrUpDate(Product product);

    List<Product> getAll();

    Optional<Product> findById(int id);
}
