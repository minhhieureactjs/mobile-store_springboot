package com.java.mobilestore.service;

import java.util.List;
import java.util.Optional;

import com.java.mobilestore.entity.CartItem;
import com.java.mobilestore.entity.User;

public interface ShoppingCartService {

	List<CartItem> listCartItems(User user);

	List<CartItem> getAllItems();

	Optional<CartItem> findItemById(int id);

	CartItem save(CartItem item);

	List<CartItem> addToCart(int proId, User user);


	void removeItem(int id);

	void clearCart();

}
