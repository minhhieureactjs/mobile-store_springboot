package com.java.mobilestore.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import com.java.mobilestore.entity.CartItem;
import com.java.mobilestore.entity.Product;
import com.java.mobilestore.entity.User;
import com.java.mobilestore.repository.CartItemRespository;
import com.java.mobilestore.repository.ProductRepository;
import com.java.mobilestore.repository.UserRepository;
import com.java.mobilestore.service.ShoppingCartService;

@SessionScope
@Service
public class ShoppingCartServiceImp implements ShoppingCartService {

	@Autowired
	private CartItemRespository cartRepo;
	
	@Autowired
	private ProductRepository proRebo;
	
	@Autowired
	private UserRepository userRebo;

	@Override
	public List<CartItem> listCartItems(User user) {
		return cartRepo.findByUser(user);
	}
	
	@Override
	public List<CartItem> getAllItems() {
		return cartRepo.findAll();
	}

	@Override
	public void removeItem(int id) {
		cartRepo.deleteById(id);
	}

	@Override
	public void clearCart() {
		cartRepo.deleteAll();
	}

	@Override
	public CartItem save(CartItem item) {
		return cartRepo.save(item);
	}

	@Override
	public Optional<CartItem> findItemById(int id) {
			return cartRepo.findById(id);
	}

	@Override
	public List<CartItem> addToCart(int proId, User user) {

		Product product = proRebo.findById(proId).get();

		List<CartItem> list = cartRepo.findByUser(user);

		var count = 0;
		for (var item : list) {
			if (item.getProduct().getId() == proId) {
				item.setQuantity(item.getQuantity() + 1);
				cartRepo.save(item);
			} else {
				count++;
			}
		}
		if (list.size() == count) {
			CartItem newItem = new CartItem();
			newItem.setProduct(product);
			newItem.setUser(user);
			newItem.setQuantity(1);
			cartRepo.save(newItem);
		}
		return cartRepo.findByUser(user);
	}
}
