package com.java.mobilestore.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.mobilestore.entity.User;
import com.java.mobilestore.repository.UserRepository;
import com.java.mobilestore.service.UserService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserRepository userRepository;
	@Override
	public List<User> getAll() {
		return userRepository.findAll();
	}
	@Override
	public Optional<User> getUserById(int id) {
		return userRepository.findById(id);
	}


}
