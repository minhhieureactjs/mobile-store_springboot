package com.java.mobilestore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.java.mobilestore.entity.CartItem;
import com.java.mobilestore.entity.Product;
import com.java.mobilestore.entity.User;

@Repository
public interface CartItemRespository extends JpaRepository<CartItem, Integer> {

	List<CartItem> findByUser(User user);
	
	CartItem findByProductId(Product product);
}
