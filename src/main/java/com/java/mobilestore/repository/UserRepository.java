package com.java.mobilestore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.java.mobilestore.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}
