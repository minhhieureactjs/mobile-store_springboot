package com.java.mobilestore.controller;


import com.java.mobilestore.entity.Product;
import com.java.mobilestore.request.ProductRequest;
import com.java.mobilestore.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.java.mobilestore.entity.Product;
import com.java.mobilestore.service.ProductService;

import javax.swing.text.html.Option;

@CrossOrigin(origins = "http://127.0.0.1:5500")
@RestController
@RequestMapping("api")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/admin/product/all")
    public ResponseEntity<List<Product>> getAllProduct() {
        try {
            List<Product> listProduct = productService.getAll();
            return new ResponseEntity<>(listProduct, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<Product> createProduct(@RequestBody ProductRequest request) {
        try {
            Product product = new Product();
            product.setProductName(request.getProductName());
            product.setUnitPrice(request.getUnitPrice());
            product.setUnitInStock(request.getUnitInStock());
            product.setDescription(request.getDescription());
            product.setManufacturer(request.getManufacturer());
            product.setCategory(request.getCategory());
            product.setCondition(request.getCondition());
            product.setImage(request.getImage());
            return new ResponseEntity<>(productService.saveOrUpDate(product), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("/findById")
    public ResponseEntity<Optional<Product>> getProductDetailById(@RequestParam int id)
    {
        if(productService.findById(id).isPresent()){
            return new ResponseEntity<>(productService.findById(id),HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
