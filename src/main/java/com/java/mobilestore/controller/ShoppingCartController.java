package com.java.mobilestore.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.java.mobilestore.entity.CartItem;
import com.java.mobilestore.entity.User;
import com.java.mobilestore.service.ShoppingCartService;
import com.java.mobilestore.service.UserService;

@RestController
@RequestMapping("/api")
public class ShoppingCartController {
	
	@Autowired
	private ShoppingCartService cartService;
	
	@Autowired 
	private UserService userService;
	
	
	@GetMapping("/cart")
	public ResponseEntity<List<CartItem>> getAllItems(){
		Optional<User> optionalUser = userService.getUserById(2);
		return new ResponseEntity<>(cartService.listCartItems(optionalUser.get()),HttpStatus.OK);
	}
	
	@PostMapping("/cart/{id}")
	public ResponseEntity<List<CartItem>> addItem(@PathVariable("id") int id){
		Optional<User> user = userService.getUserById(2);
		List<CartItem> listItems = cartService.addToCart(id, user.get());;
		return new ResponseEntity<>(listItems, HttpStatus.OK);
	}
	
	@DeleteMapping("/cart/{id}")
	public ResponseEntity<CartItem> deleteItem(@PathVariable int id){
		Optional<CartItem> optional = cartService.findItemById(id);
		return optional.map(item ->{
			cartService.removeItem(id);
			return new ResponseEntity<>(item, HttpStatus.OK);
		}).orElseGet(()-> new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
	
	@DeleteMapping("/cart/clear")
	public void clearCart(){
		cartService.clearCart();
	}
}
